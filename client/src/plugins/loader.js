// loader plugin to show a bar at the top when getting data from api
const loader = document.getElementById('loader')

function showLoader() {
  loader.classList.remove('loader--hide')
}

function hideLoader() {
  loader.classList.add('loader--hide')
}

export default { showLoader, hideLoader }

