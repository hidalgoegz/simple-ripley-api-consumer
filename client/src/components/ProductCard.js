import React from 'react'
import { Link } from 'react-router-dom'

// A little card made with bootstrap to show products in the catalog
function ProductCard({ product }) {
  return (
    <div className="card mt-4" style={{ "max-width": '540px' }}>
      <div className="row no-gutters align-items-center" style={{ "min-height": '200px' }}>
        <div class="col-md-4">
          <img src={'https:' + product.thumbnailImage} alt={product.shortDescription} className="card-img img-fluid" />
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title">
              <Link to={'/' + product.partNumber}>
                {product.name}
              </Link>
            </h5>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductCard
