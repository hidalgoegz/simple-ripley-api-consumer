import React, { Component } from 'react'
import axios from 'axios'
import loader from './../plugins/loader'

// component to show individual products
class Product extends Component {
  constructor() {
    super();
    // show loading bar at start of loading
    loader.showLoader()
  }

  state = {
    product: {}
  }

  componentDidMount() {
    const sku = this.props.match.params.sku // alias for url param

    axios
      .get('/api/products/' + sku)
      .then(response => response.data.data)
      .then(product => {
        // never set a state directly!!
        const newState = Object.assign({}, this.state, {
          product: product[0]
        })

        this.setState(newState)
        // hide loading bard after succefully load
        loader.hideLoader()
      })
      .catch(error => console.log(error))
  }

  render() {
    const product = this.state.product // alias for state data

    return (
      <div className="row">
        <div className="col-12">
          <div className="card">
            <img src={'https:' + product.fullImage} alt={product.shortDescription} className="card-img-top img-fluid" style={{ 'max-height': '340px' }}/>
            <div className="card-body">
              <h5 className="card-title">{product.name}</h5>
              <p className="card-text" dangerouslySetInnerHTML={{ __html: product.longDescription}}></p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Product
