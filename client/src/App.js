import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './components/Header'
import Catalog from './components/Catalog'
import Product from './components/Product'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="container">
        <Router>
          <Header />

          <Route exact path="/" component={Catalog} />
          <Route path="/:sku" component={Product} />
        </Router>
      </div>
    )
  }
}

export default App
