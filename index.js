const express = require('express')
const path = require('path')
const routes = require('./apiRoutes')

// Create express application instance
const app = express()

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')))

// Put all API endpoints under '/api'
app.use('/api', routes)

// Handler to catch any request that are not for the api
// Send everything to React app
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/build/index.html'))
})

// Use heroku port or 5000 by default
const port = process.env.PORT || 5000
app.listen(port)

console.log(`Server listening on ${port}`)

