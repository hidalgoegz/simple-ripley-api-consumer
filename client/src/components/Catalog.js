import React, { Component } from 'react'
import ProductCard from './ProductCard'
import axios from 'axios'
import loader from './../plugins/loader'

// list with the 25 default products
class Catalog extends Component {
  constructor() {
    super();
    // show loading bar at start of loading
    loader.showLoader()
  }

  state = {
    products: []
  }

  componentDidMount() {
    axios
      .get('/api/products')
      .then(response => response.data.data)
      .then(products => {
        // never set a state directly!!
        const newState = Object.assign({}, this.state, {
          products: products
        })

        this.setState(newState)
        // hide loading bard after succefully load
        loader.hideLoader()
      })
      .catch(error => console.log(error))
  }

  render() {
    const products = this.state.products // alias for state data
    return (
      <div className="row justify-content-around">
        {products.map((product, _index) => (
          <div className="col-md-6 col-lg-4">
            <ProductCard product={product} />
          </div>
        ))}
      </div>
    )
  }
}

export default Catalog
