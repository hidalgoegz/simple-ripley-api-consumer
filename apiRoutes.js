const express = require('express')
const axios = require('axios')
const redis = require('redis')
const router = express.Router()
const config = require('./config/index') // default configurations

// Set default for axios
axios.defaults.baseURL = config.apiUrl

// Create and connect redis client to local instance or heroku redis
const client = redis.createClient(process.env.REDIS_URL || 6379)

// Echo redis errors
client.on('error', (err) => {
  console.log('Error: ' + err)
});

// GET /api/products
router.get('/products', (req, res) => {
  const productsRedisKey = 'products'

  // get data from redis if not exists get from ripley api and save
  return client.get(productsRedisKey, (err, products) => {
    if (products) {
      return res.json({ source: 'cache', data: JSON.parse(products) })
    } else {
      get_product(res)
    }
  })
})

// GET /api/products/:sku
router.get('/products/:sku', (req, res) => {
  let sku = req.params.sku

  // get data from redis if not exists get from ripley api and save
  return client.get(sku, (err, product) => {
    if (product) {
      return res.json({ source: 'cache', data: JSON.parse(product)})
    } else {
      get_product(res, sku)
    }
  })
})

// method to centralize data getter
function get_product(res, sku = undefined, tries = 1) {
  // if sku is setted get product by sku, else get 25 products by default
  const redisKey = sku || 'products'

  axios
    .get('/products', {
      params: {
        partNumbers: sku || config.defaultProducts.join(','),
        format: 'json'
      }
    })
    .then((response, promise) => {
      // throw error with 10% chance (not sure if done correctly)
      let randomNumber = Math.random()
      let chance = 0.1

      // to avoid improbable but posible infinite tries don't throw error after 5 tries
      if (tries < 5 && chance <= randomNumber) {
        throw new Error('Random error')
      }

      return response.data
    })
    .then(data => {
      // set data after is getted from ripley api
      client.set(redisKey, JSON.stringify(data), 'EX', 60, (error, reply) => {
        return res.json({ source: 'api', data: data })
      })
    })
    .catch(error => {
      console.log(error)
      console.log('Number of tries: ' + tries)
      // Retry until there is no error
      setTimeout(() => {
        get_product(res, sku, ++tries)
      }, 1000)
    })
}

module.exports = router

