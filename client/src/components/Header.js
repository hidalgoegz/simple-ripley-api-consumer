import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

// Simple header to show a link to easy access home
function Header() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link to="/" className="navbar-brand">Inicio</Link>
    </nav>
  )
}

export default Header
