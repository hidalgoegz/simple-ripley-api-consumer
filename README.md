# Introducción

Esta aplicación es un simple consumidor de la **API** `https://simple.ripley.cl/api/v2`

# Desarrollo

Pasos para montar este proyecto en un ambiente de desarrollo local

- Descargar este repositorio
- `cd simple-ripley-api-consumer`
- `yarn install` // instalar dependencias de la **API**
- `cd client && yarn install` // Instalar dependencias del front
- `cd ..`
- `yarn start` // con esto se monta la **API** en localhost:5000
- `yarn start-front` // con esto se monta el front en localhost:3000

Para hacer un test de la **API** usar el comando `yarn test`.

# Producción

Es necesario para lanzar a producción tener una cuenta en Heroku con un metodo de pago seleccionado (Es completamente gratuito, solo requiere tener una opcion de pago dentro de su cuenta).

Utilizando el CLI de heroku debe hacer lo siguiente

- `heroku create`
- `git push heroku master`
- Asegurese de colocar el plugin de `redis` dentro del la aplicación en su cuenta heroku

Listo al terminar le debe de aparecer un link donde la aplicación estara montada
