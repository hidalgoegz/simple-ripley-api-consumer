const supertest = require('supertest')
const should = require('should')

// Listen to server
const server = supertest.agent('http://localhost:5000/api');

// Unit tests

describe('GET /products', () => {
  it('should return 25 products', done => {
    server
      .get('/products')
      .expect('Content-type',/json/)
      .end((error, response) => {
        response.status.should.equal(200)

        // Should respond with 25 products
        response.body.should.have.property('data').with.lengthOf(25)

        // Complete test
        done()
      })
  }).timeout(10000) // add more timeout because random mistake chances
})

describe('GET /products/231928', () => {
  it('should return a product by part number', done => {
    server
      .get('/products/231928')
      .expect('Content-type',/json/)
      .end((error, response) => {
        response.status.should.equal(200)

        // Should respond with a product with the same part number
        response.body.should.have.property('data').with.lengthOf(1)
        response.body.data[0].should.have.property('partNumber').equal('231928')

        // Complete test
        done()
      })
  }).timeout(10000) // add more timeout because random mistake chance
})
